// Declaración de Variables
variable "DIGITALOCEAN_TOKEN_MTWDM" {}

// Definición del Proveedor Cloud
terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
  backend "http" {}
}

// Token de Autenticación del Proveedor Cloud
provider "digitalocean" {
  token = var.DIGITALOCEAN_TOKEN_MTWDM
}

// Recurso Cluster de Kubernetes
resource "digitalocean_kubernetes_cluster" "devsecops_k8s" {
  name    = "devsecopscluster"
  region  = "sfo3"
  version = "1.27.4-do.0"

  node_pool {
    name       = "devsecops-nodes"
    size       = "s-4vcpu-8gb-intel" //s-1vcpu-2gb | s-4vcpu-8gb-intel
    auto_scale = true
    node_count = 3
    min_nodes  = 3
    max_nodes  = 3
  }
}

provider "kubernetes" {
  # config_path = local_file.k8s_config.filename
  host = digitalocean_kubernetes_cluster.devsecops_k8s.endpoint
  cluster_ca_certificate = base64decode(
    digitalocean_kubernetes_cluster.devsecops_k8s.kube_config[0].cluster_ca_certificate
  )
  token = digitalocean_kubernetes_cluster.devsecops_k8s.kube_config.0.token

  exec {
    api_version = "client.authentication.k8s.io/v1beta1"
    command     = "doctl"
    args = ["kubernetes", "cluster", "kubeconfig", "exec-credential",
    "--version=v1beta1", digitalocean_kubernetes_cluster.devsecops_k8s.id]
  }
}

// Crear NameSpace Dev
resource "kubernetes_namespace" "ns_devsecops_dev" {
  metadata {
    name = "devsecops-dev"
  }
}

// Crear NameSpace QAs
resource "kubernetes_namespace" "ns_devsecops_qas" {
  metadata {
    name = "devsecops-qas"
  }
}

// Crear NameSpace Pro
resource "kubernetes_namespace" "ns_devsecops_pro" {
  metadata {
    name = "devsecops-pro"
  }
}

// Crear NameSpace Paulina
resource "kubernetes_namespace" "ns_devsecops_feature_paulina" {
  metadata {
    name = "devsecops-feature-paulina"
  }
}

// Crear NameSpace Hugo
resource "kubernetes_namespace" "ns_devsecops_feature_hugo" {
  metadata {
    name = "devsecops-feature-hugo"
  }
}

// Crear NameSpace Tere
resource "kubernetes_namespace" "ns_devsecops_feature_tere" {
  metadata {
    name = "devsecops-feature-tere"
  }
}

// Crear NameSpace Jonathan
resource "kubernetes_namespace" "ns_devsecops_feature_jonathan" {
  metadata {
    name = "devsecops-feature-jonathan"
  }
}